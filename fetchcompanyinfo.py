import clearbit
import os
import pandas
import urllib

clearbit.key = 'sk_5fe65df8ad7419540dfb819ef0498020'
source_file = 'companies.csv'
temp_file = 'companies-t.csv'
downloads_DIR = 'logos/'

# Read CSV
dataFrame = pandas.read_csv(source_file, index_col='Company Name')

for index, row in dataFrame.iterrows():
    company = clearbit.NameToDomain.find(name=index, stream=True)
    if company != None:
        companyEnriched = clearbit.Company.find(
            domain=company['domain'], stream=True)
        dataFrame.loc[index, "Company Domain"] = company['domain']
        dataFrame.loc[index, "Company Logo"] = companyEnriched['logo']
        dataFrame.loc[index, "Company Searched Name"] = companyEnriched['name']
        dataFrame.loc[index,
                      "Company Legal Name"] = companyEnriched['legalName']
        url = company['logo']
        if not os.path.exists(downloads_DIR):
          os.makedirs(downloads_DIR)
        filename = os.path.join(downloads_DIR, (company['name'] + ".jpg") )
        if not os.path.isfile(filename):
            print('Downloading: ' + filename)
            try:
                urllib.request.urlretrieve(url, filename)
            except Exception as inst:
                print(inst)
                print('  Encountered unknown error. Continuing.')
    else:
        dataFrame.loc[index, "Company Legal Name"] = "NA"
        dataFrame.loc[index, "Company Searched Name"] = "NA"
        dataFrame.loc[index, "Company Domain"] = "NA"
        dataFrame.loc[index, "Company Logo"] = "NA"

print (dataFrame)

dataFrame.to_csv(temp_file)
os.remove(source_file)
os.rename(temp_file, source_file)
